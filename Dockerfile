FROM python:3
ADD bin/* /opt/dk/cf_helper/bin
ADD cf_helper/* /opt/dk/cf_helper/cf_helper
RUN pip install cloudflare
RUN pip install -e .
CMD [ "cf_helper" ]