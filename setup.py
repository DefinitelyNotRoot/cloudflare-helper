from setuptools import setup

setup(name='cf_helper',
      version='0.1',
      description='xd',
      url='xd',
      author='xd',
      author_email='dkalicki',
      license='MIT',
      packages=['cf_helper'],
      entry_points={
          'console_scripts': [
              'cf_helper=cf_helper.__main__:main'
          ]
      },
      install_requries=[
          'cloudflare'],
      zip_safe=False)
