import CloudFlare
import pprint
import sys
import json
import argparse
import logging
import os


def importConfig():
    with open(os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'config.json'))) as json_config_file:
        config = json.load(json_config_file)
    return config


def getAccounts(config):
    logging.debug("Account list: {}".format(config.get('accounts')))
    return config.get('accounts')


def getAccount(account_id,config):
    account=getAccounts(config)[account_id]
    logging.debug("Account you chose: {}".format(account))
    return account.get('email'), account.get('api_token')

def listZoneID(cf, name=None):
    if name:
        zones = cf.zones.get(params={'name': name, 'per_page': 1})
        logging.debug("Zone id: {}".format(zones[0]))
        return zones
    else:
        zones = cf.zones.get(params={'per_page': 100})
        zones_dict = {}
        for zone in zones.get('result'):
            zones_dict[zone.get('name')] = zone['id']
        logging.debug("Zone dict: {}".format(zones_dict))
    return zones_dict


def getZoneID(cf, name):
    zones = cf.zones.get(params={'name': name, 'per_page': 1})
    logging.debug(zones)
    return zones.get('result')[0].get('id')


def clearCache(cf, zone_id):
    return cf.zones.purge_cache.delete(zone_id, data={"purge_everything": True})


def PageRuleList(cf, zone_id):
    return cf.zones.pagerules.get(zone_id)


def PageRulesDetails(cf, zone_id, page_rule_id):
    details = cf.zones.pagerules.get(zone_id, page_rule_id)
    logging.debug("PageRulesDetails: {}".format(details))
    return details.get('result').get('id'), details.get('result').get('targets'), details.get('result').get('actions'), details.get('result').get('status')


def PageRulesUpdate(cf, zone_id, page_rule_id, page_rule_from, page_rule_to, status, confirm=True):
    data = {
        'targets': [
            {
                "target": "url",
                "constraint": {
                    "operator": "matches",
                    "value": page_rule_from
                }
            }
        ],
        'actions': [
            {
                'id': 'forwarding_url',
                'value': {
                    'status_code': 301,
                    'url': page_rule_to
                }
            }

        ],
        'priority': 1,
        'status': status
    }
    logging.debug("Data body is: {}".format(data))
    human_confirmation(confirm)
    output = cf.zones.pagerules.put(
        identifier1=zone_id, identifier2=page_rule_id, data=data)
    return output


def human_confirmation(confirm):
    yes = {'yes', 'y'}
    no = {'no', 'n'}
    if confirm:
        choice = input("Please confirm (y/Y or n/N): ").lower()
        if choice in yes:
            pass
        elif choice in no:
            sys.exit(1)
        else:
            print("Wrong option")
            sys.exit(1)


def main():

    pp = pprint.PrettyPrinter(indent=4)
    parser = argparse.ArgumentParser(
        description='Python CloudFlare Wrapper for ACN/BGE')
    parser.add_argument("--config",
                        help="url argument is requried for recognize zone id. ", default=False, action="store_true")
    parser.add_argument("-aid", "--account_id",
                        help="details about page rules",  default=True, action="store", type=int)
    parser.add_argument("-url", "--url",
                        help="url argument is requried for recognize zone id. ", default=None, action="store")
    parser.add_argument(
        "-l", "--list", help="Show list of zones id and sites", action="store_true", default=False)
    parser.add_argument(
        "-c", "--clearchace", help="clearing the caches for specific url. Ex. main.py -c myacn.eu | output: return result id", action="store_true", default=False)
    parser.add_argument(
        "-gz", "--getzoneid", help="getting zone id for specific url. Ex. main.py -gz myacn.eu -> return zone id", action="store_true", default=False)
    parser.add_argument("-prl", "--pagerulelist",
                        help="details about page rules", action="store_true", default=False)
    parser.add_argument("-prd", "--pageruledetails",
                        help="To check details please list before and pick ID", action="store", default=False, nargs=1)
    parser.add_argument("-pru", "--pageruleupdate",
                        help="Update the page rules. Please add arguments -f -t -s:", action="store", nargs=1,)
    parser.add_argument("-f", "--rulefrom",
                        help="details about page rules", action="store", nargs=1, )
    parser.add_argument("-t", "--ruleto",
                        help="details about page rules",  action="store",  nargs=1)
    parser.add_argument("-s", "--status",
                        help="details about page rules",  action="store", nargs=1,)



    parser.add_argument("-v", "--version",
                        help="show program's version number and exit",  action="version", default=False, version="CloudFlare Helper: 0.1a")

    args = parser.parse_args()
    config=importConfig()
    if args.config:
        getAccounts(config)
        sys.exit(0)
    if  (args.account_id or args.account_id ==0):
        email, api_token = getAccount(args.account_id,config)
        cf = CloudFlare.CloudFlare(
        email=email, token=api_token, raw=True)
        if args.list:

            pp.pprint(listZoneID(cf))
        if args.url:
            logging.debug("Url is: {}".format(args.url))
            if args.getzoneid:
                pp.pprint(getZoneID(cf, args.url))
            if args.clearchace:
                pp.pprint(clearCache(cf, getZoneID(cf, args.url)))
            if args.pagerulelist:
                pp.pprint(PageRuleList(cf, getZoneID(cf, args.url)))
            if args.pageruledetails:
                pp.pprint(PageRulesDetails(cf, getZoneID(
                    cf, args.url), args.pageruledetails))

            if args.pageruleupdate:
                if args.rulefrom is not None and args.ruleto is not None and args.status is not None:
                    print("Page Rule id: {}, From {}, to: {}, status: {}.".format(
                        args.pageruleupdate[0], args.rulefrom[0], args.ruleto[0], args.status[0]))
                    pp.pprint(PageRulesUpdate(cf=cf, zone_id=getZoneID(
                        cf, args.url), page_rule_id=args.pageruleupdate[0], page_rule_from=args.rulefrom[0], page_rule_to=args.ruleto[0], status=args.status[0]))
                else:
                    parser.error(
                        "RuleFrom, RuleTo, Status are neccessary to run pageruleupdate")
    
    else:
        parser.print_help()
        parser.error(
            "One option like -g / -c / -pdl / -prd / -pru is needed. While url is provided.")
        sys.exit(1)


if __name__ == '__main__':
    logging.basicConfig(level=logging.CRITICAL)
    main()
